<?php

use App\Person; //for introducing the class
use App\Student;

//autoload called to load this function again & again. If we've 100 class, then autoload will generate 100 times.
function __autoload($className){

    //echo $className;
    list ($ns, $cn) = explode("\\",$className);//listed namespace & classname and exploding backslashes to find the className.
    require_once("../../src/BITM/SEIP_143262/".$cn.".php");
}


//require_once("../../src/BITM/SEIP_143262/Person.php"); //declaring the classpath. Classpaths must be declared once only.
//require_once("../../src/BITM/SEIP_143262/Student.php");

$obj = new Person();

echo $obj -> ShowInfo();

$obj1 = new Student();

echo $obj1 -> ShowstuInfo();


?>